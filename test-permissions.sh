#!/bin/bash

HTTP_OPTS="--verify=no --timeout=1800 --pretty=format"

timestamp() {
    echo $(gdate "+%Y-%m-%d %H:%M:%S.%N" | cut -b1-23)
}

clean() {
    echo ${@} | sed -E 's/^0+//' | sed -E 's/[ :.-]//g'
}

httpCall() {
    local SENDER=${1}
    local URL=${SERVER}/xapi${2}
    echo "${SENDER} -> ${URL}: $(http --session=${SENDER} ${HTTP_OPTS} --print=h ${URL} | grep -E '^HTTP/1\.1')"
}    

extract() {
    local TARGET=${1}
    local URL=${2}
    [[ ${URL} =~ ^.*/${TARGET}/[^/]+/.*$ ]] && { echo ${URL} | sed -E 's#^.*/'${TARGET}'/([^/]+)/.*$#\1#'; } || { [[ ${TARGET} != "operation" ]] && { echo "x"; } || { echo $(basename ${URL}); }; }
}

storeData() {
    local SENDER=${1}
    local URL=${2}
    local RESULT=${3}
    local PROJECT=$(extract projects ${URL})
    local SUBJECT=$(extract subjects ${URL})
    local EXPERIMENT=$(extract experiments ${URL})
    local OPERATION=$(extract operation ${URL})
    printf '%s,%s,%s,%s,%s,%s,%s\n' ${SENDER} ${URL} ${RESULT} ${PROJECT} ${SUBJECT} ${EXPERIMENT} ${OPERATION} | sed -E "s/,x/,/g" >> ${SQL_FILE}
}

operations() {
    local URL=${1}
    for OPERATION in read edit delete; do
        echo "${URL}/${OPERATION}"
    done
}

count() {
    for SENDER in ${SENDERS[*]}; do
        operations ${SENDER} /test/restrictTo/projects/Public
        operations ${SENDER} /test/restrictTo/projects/Public/subjects/Public_01
        operations ${SENDER} /test/restrictTo/projects/Public/subjects/Public_01/experiments/Public_01_MR_01
        for PROJECT in ${PROJECTS[*]}; do
            operations ${SENDER} /test/restrictTo/projects/${PROJECT}
            for SUBJECT in ${SUBJECTS[*]}; do
                operations ${SENDER} /test/restrictTo/projects/${PROJECT}/subjects/${SUBJECT}
                for EXPERIMENT in ${EXPERIMENTS[*]}; do
                    operations ${SENDER} /test/restrictTo/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${EXPERIMENT}
                done
            done
            for EXPERIMENT in ${EXPERIMENTS[*]}; do
                operations ${SENDER} /test/restrictTo/projects/${PROJECT}/experiments/${EXPERIMENT}
            done
        done
        for SUBJECT in ${SUBJECTS[*]}; do
            operations ${SENDER} /test/restrictTo/subjects/${SUBJECT}
            for EXPERIMENT in ${EXPERIMENTS[*]}; do
                operations ${SENDER} /test/restrictTo/subjects/${SUBJECT}/experiments/${EXPERIMENT}
            done
        done
        for EXPERIMENT in ${EXPERIMENTS[*]}; do
            operations ${SENDER} /test/restrictTo/experiments/${EXPERIMENT}
        done
    done | wc -l
}
    
runTest() {
    local SENDER=${1}
    local URL=${2}
    for OPERATION in $(operations ${URL}); do
        local OUTPUT=$(httpCall ${SENDER} ${OPERATION})
        local RESULT=$(echo ${OUTPUT} | sed -E 's#^.*HTTP/1\.1 ##' | cut -f 1 -d " ")
        storeData ${SENDER} ${OPERATION} ${RESULT}
        echo ${OUTPUT}
    done
}

[[ -z ${1} || ! ${1} =~ ^--server=.*$ ]] && { SERVER="https://xnatdev.xnat.org"; } || { SERVER=$(echo ${1} | cut -f 2 -d =); shift; }
[[ -z ${1} ]] && { SENDERS=(admin dataAdmin dataAccess owner01 member01 collab01 owner02 member02 collab02); } || { SENDERS=("${@}"); }

PROJECTS=(XNAT_01 XNAT_02)
SUBJECTS=(XNAT_S00001 XNAT_S00002 XNAT_01_01 XNAT_01_02 XNAT_02_01)
EXPERIMENTS=(XNAT_E00001 XNAT_E00002 XNAT_01_01_MR_01 XNAT_01_02_MR_01 XNAT_02_01_MR_01)

START=$(timestamp)
SCRIPT_NAME=$(echo "${0%.*}" | sed -E 's#^\./##')
TIMESTAMP=$(clean ${START})
LOG_FILE="exec-${SCRIPT_NAME}-${TIMESTAMP}.log"
SQL_FILE="results-${SCRIPT_NAME}-${TIMESTAMP}.sql"
TOUCH_FILE=".${SCRIPT_NAME}-${TIMESTAMP}.txt"
TOTAL_COUNT=$(count)
echo ${TOTAL_COUNT} > ${TOUCH_FILE}

cat > ${SQL_FILE} << "ENDSQL"
DROP TABLE IF EXISTS test_results;
CREATE TABLE test_results
(
	id SERIAL PRIMARY KEY,
	username VARCHAR(15) NOT NULL,
	url VARCHAR(255) NOT NULL,
	result INTEGER NOT NULL,
	project VARCHAR(15),
	subject VARCHAR(15),
	experiment VARCHAR(31),
	operation VARCHAR(15) NOT NULL
);
COPY test_results (username, url, result, project, subject, experiment, operation) FROM stdin WITH (FORMAT csv);
ENDSQL

exec > >(tee -i ${LOG_FILE})
exec 2>&1

echo "Testing on ${SERVER} with users:"
echo

for SENDER in ${SENDERS[*]}; do
    echo " * ${SENDER}"
done

echo
echo "Preparing to perform ${TOTAL_COUNT} total operations"
echo
echo "Starting at ${START}, logging all output to ${LOG_FILE}"
echo "Touch: ${TOUCH_FILE}"
echo

for SENDER in ${SENDERS[*]}; do
    http --auth=${SENDER}:${SENDER} --session=${SENDER} ${HTTP_OPTS} --print=h ${SERVER}/data/JSESSION | grep -E '^HTTP/1\.1'

    # Run tests against public project first
    runTest ${SENDER} /test/restrictTo/projects/Public
    runTest ${SENDER} /test/restrictTo/projects/Public/subjects/Public_01
    runTest ${SENDER} /test/restrictTo/projects/Public/subjects/Public_01/experiments/Public_01_MR_01
    
    for PROJECT in ${PROJECTS[*]}; do
        runTest ${SENDER} /test/restrictTo/projects/${PROJECT}
        for SUBJECT in ${SUBJECTS[*]}; do
            runTest ${SENDER} /test/restrictTo/projects/${PROJECT}/subjects/${SUBJECT}
            for EXPERIMENT in ${EXPERIMENTS[*]}; do
                runTest ${SENDER} /test/restrictTo/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${EXPERIMENT}
            done
        done
        for EXPERIMENT in ${EXPERIMENTS[*]}; do
            runTest ${SENDER} /test/restrictTo/projects/${PROJECT}/experiments/${EXPERIMENT}
        done
    done
    for SUBJECT in ${SUBJECTS[*]}; do
        runTest ${SENDER} /test/restrictTo/subjects/${SUBJECT}
        for EXPERIMENT in ${EXPERIMENTS[*]}; do
            runTest ${SENDER} /test/restrictTo/subjects/${SUBJECT}/experiments/${EXPERIMENT}
        done
    done
    for EXPERIMENT in ${EXPERIMENTS[*]}; do
        runTest ${SENDER} /test/restrictTo/experiments/${EXPERIMENT}
    done
    http --session=${SENDER} ${HTTP_OPTS} --print=h DELETE ${SERVER}/data/JSESSION | grep -E '^HTTP/1\.1'
done

echo "\." >> ${SQL_FILE}

FINISH=$(timestamp)
ELAPSED=$(( $(clean ${FINISH}) - $(clean ${START}) ))
echo
echo "Finished at ${FINISH}, total elapsed time ${ELAPSED} ms"
rm -f ${TOUCH_FILE}

echo 
unset ELAPSED EXPERIMENT FINISH HTTP_OPTS LOG_FILE PROJECT SENDER SENDERS SERVER SQL_FILE START SUBJECT

