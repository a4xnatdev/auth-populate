#!/bin/bash

[[ -z ${1} ]] && { SERVER="https://xnatdev.xnat.org"; } || { SERVER=${1}; }

. $(dirname ${0})/macros.sh

initialize

addUser dataAdmin ALL_DATA_ADMIN
addUser dataAccess ALL_DATA_ACCESS

addUser owner01
addUser member01
addUser collab01
addUser owner02
addUser member02
addUser collab02

authUser owner01
authUser owner02

addProject owner01 XNAT_01
addProjectUser owner01 XNAT_01 member01 member
addProjectUser owner01 XNAT_01 collab01 collaborator
FIRST_SUBJECT_ID=$(trim $(addSubject owner01 XNAT_01 XNAT_01_01 | fgrep XNAT_S))
sendMrSession owner01 XNAT_01 XNAT_01_01 XNAT_01_01_MR_01

addProject owner02 XNAT_02
addProjectUser owner02 XNAT_02 member02 member
addProjectUser owner02 XNAT_02 collab02 collaborator

shareSubject admin XNAT_01 XNAT_01_01 XNAT_02 XNAT_02_01
FIRST_EXPERIMENT_ID=$(trim $(shareExperiment admin XNAT_01 XNAT_01_01 XNAT_01_01_MR_01 XNAT_02 XNAT_02_01_MR_01 | fgrep XNAT_E))

addUser owner03
addUser member03
addUser collab03
addUser owner04
addUser member04
addUser collab04

authUser owner03
authUser owner04

addProject admin Public public
addProject owner03 XNAT_03
addProjectUser owner03 XNAT_03 member03 member
addProjectUser owner03 XNAT_03 collab03 collaborator
SECOND_SUBJECT_ID=$(trim $(addSubject owner03 XNAT_03 XNAT_03_01 | fgrep XNAT_S))
sendMrSession owner03 XNAT_03 XNAT_03_01 XNAT_03_01_MR_01

addProject owner04 XNAT_04
addProjectUser owner04 XNAT_04 member04 member
addProjectUser owner04 XNAT_04 collab04 collaborator
addProjectUser owner04 XNAT_04 owner03 collaborator
addProjectUser owner04 XNAT_04 member03 collaborator
addProjectUser owner04 XNAT_04 collab03 collaborator

shareSubject admin XNAT_03 XNAT_03_01 XNAT_04 XNAT_04_01
SECOND_EXPERIMENT_ID=$(trim $(shareExperiment admin XNAT_03 XNAT_03_01 XNAT_03_01_MR_01 XNAT_04 XNAT_04_01_MR_01 | fgrep XNAT_E))

psql --username=xnat --host=xnatdev.xnat.org --dbname=xnat --file=test-queries.sql
psql --username=xnat --host=xnatdev.xnat.org --dbname=xnat --command="SELECT * FROM test_source_and_shared_permissions('dataAccess', 'dataAdmin', 'XNAT_01', 'owner01', 'member01', 'collab01', 'XNAT_02', 'owner02', 'member02', 'collab02', '${FIRST_SUBJECT_ID}', '${FIRST_EXPERIMENT_ID}')"
psql --username=xnat --host=xnatdev.xnat.org --dbname=xnat --command="SELECT * FROM test_permissions_with_collab_access_to_shared('owner03', 'member03', 'collab03', '${SECOND_SUBJECT_ID}', '${SECOND_EXPERIMENT_ID}', 'Public', 'XNAT_03', 'XNAT_03_01', 'XNAT_03_01_MR_01', 'XNAT_04', 'XNAT_04_01', 'XNAT_04_01_MR_01')"

