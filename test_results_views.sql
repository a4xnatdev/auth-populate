DROP VIEW IF EXISTS public.test_results_get_referenced_results;
DROP VIEW IF EXISTS public.test_results_get_invalid_coordinates;
DROP VIEW IF EXISTS public.test_results_get_valid_coordinates;
    
CREATE OR REPLACE VIEW public.test_results_get_valid_coordinates AS
SELECT DISTINCT
    project,
    subject,
    experiment
FROM
    test_results
WHERE
        project IS NULL AND
        (subject LIKE 'XNAT_S%' AND experiment IS NULL OR
         subject IS NULL AND experiment LIKE 'XNAT_E%' OR
         subject = 'XNAT_S00001' AND experiment IN ('XNAT_E00001', 'XNAT_01_01_MR_01') OR
         subject = 'XNAT_S00002' AND experiment IN ('XNAT_E00002', 'XNAT_01_02_MR_01')) OR
        project = 'XNAT_01' AND
        (subject IS NULL AND (experiment IS NULL OR experiment SIMILAR TO '(XNAT_E|XNAT_01_)%') OR
         subject SIMILAR TO '(XNAT_S|XNAT_01)%' AND experiment IS NULL OR
         subject IN ('XNAT_S00001', 'XNAT_01_01') AND experiment IN ('XNAT_01_01_MR_01', 'XNAT_E00001') OR
         subject IN ('XNAT_S00002', 'XNAT_01_02') AND experiment IN ('XNAT_01_02_MR_01', 'XNAT_E00002')) OR
        project = 'XNAT_02' AND
        (subject IS NULL OR subject IN ('XNAT_02_01', 'XNAT_S00001')) AND
        (experiment IS NULL OR experiment IN ('XNAT_E00001', 'XNAT_02_01_MR_01'))
ORDER BY
    project,
    subject,
    experiment;

CREATE OR REPLACE VIEW public.test_results_get_invalid_coordinates AS
SELECT DISTINCT
    project,
    subject,
    experiment
FROM
    test_results
WHERE
        project IS NULL AND
        (subject LIKE 'XNAT_0%' OR
         subject IS NULL AND experiment LIKE 'XNAT_0%' OR
         subject = 'XNAT_S00001' AND (experiment SIMILAR TO '(XNAT_01_02|XNAT_02_01)%' OR experiment = 'XNAT_E00002') OR
         subject = 'XNAT_S00002' AND (experiment SIMILAR TO '(XNAT_01_01|XNAT_02_01)%' OR experiment = 'XNAT_E00001')) OR
        project = 'XNAT_01' AND
        ((subject LIKE 'XNAT_02_%' OR experiment LIKE 'XNAT_02_%') OR
         subject IN ('XNAT_S00001', 'XNAT_01_01') AND experiment IN ('XNAT_01_02_MR_01', 'XNAT_02_01_MR_01', 'XNAT_E00002') OR
         subject IN ('XNAT_S00002', 'XNAT_01_02') AND experiment IN ('XNAT_01_01_MR_01', 'XNAT_E00001')) OR
        project = 'XNAT_02' AND
        (subject IS NULL AND (experiment LIKE 'XNAT_01_%' OR experiment = 'XNAT_E00002') OR
         (subject = 'XNAT_S00002' OR subject LIKE 'XNAT_01_%' OR experiment = 'XNAT_E00002' OR experiment LIKE 'XNAT_01_%'))
ORDER BY
    project,
    subject,
    experiment;

CREATE OR REPLACE VIEW public.test_results_get_referenced_results AS
    WITH
        coordinates AS
            (SELECT
                 'valid' AS state,
                 coalesce(project, 'null') AS project,
                 coalesce(subject, 'null') AS subject,
                 coalesce(experiment, 'null') AS experiment
             FROM
                 test_results_get_valid_coordinates
             UNION
             SELECT
                 'invalid' AS state,
                 coalesce(project, 'null') AS project,
                 coalesce(subject, 'null') AS subject,
                 coalesce(experiment, 'null') AS experiment
             FROM
                 test_results_get_invalid_coordinates),
        results AS
            (SELECT DISTINCT
                 coalesce(project, 'null') AS project,
                 coalesce(subject, 'null') AS subject,
                 coalesce(experiment, 'null') AS experiment,
                 username,
                 operation,
                 result
             FROM
                 test_results)
    SELECT
        r.username,
        r.project,
        r.subject,
        r.experiment,
        r.operation,
        r.result,
        c.state
    FROM
        results r
        LEFT JOIN coordinates c ON r.project = c.project AND r.subject = c.subject AND r.experiment = c.experiment;
