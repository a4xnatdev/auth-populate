#!/bin/bash

totalOperations() {
    echo "$(grep -E "^[A-z0-9]+ ->" ${CURRENT_LOG} | wc -l)"
}

[[ $(find . -mindepth 1 -maxdepth 1 -type f -name "*.log" | wc -l | tr -d ' ') == "0" ]] && { echo "No log file, I don't have anything to monitor."; exit -1; }
CURRENT_LOG=$(ls -t *.log | head -n 1)
TOUCH_FILE=$(fgrep Touch ${CURRENT_LOG} | cut -f 2 -d " ")

[[ -f ${TOUCH_FILE} ]] && {
    echo "Completed $(totalOperations) out of $(cat ${TOUCH_FILE}) operations total"
    echo
    CURRENT_SENDER=$(grep -E "^[A-z0-9]+ ->" ${CURRENT_LOG} | tail -n 1 | cut -f 1 -d " ")
    echo "Current user: ${CURRENT_SENDER}"
    echo "Current operations: $(grep -E "^[A-z0-9]+ ->" ${CURRENT_LOG} | wc -l)"
} || {
    echo "Processing completed"
    echo "Total operations: $(totalOperations)"
}
echo
echo "Distinct users and operation counts:"
echo
grep -E "^[A-z0-9]+ ->" ${CURRENT_LOG} | cut -f 1 -d " " | sort -u | \
while read SENDER; do
    echo " * ${SENDER}: $(grep -E "^${SENDER} ->" ${CURRENT_LOG} | wc -l)"
done
echo
echo "Distinct statuses:"
echo
grep -E "^[A-z0-9]+ ->" ${CURRENT_LOG} | sed -E 's#^.* HTTP/1\.1 ##' | sort -u | \
while read STATUS; do
    echo " * ${STATUS}"
done
echo

